module.exports = {
  port: 4222,
  name: "TIMERS_SERVICE",
  retryAttempts: 5,
  retryDelay: 3000,
  hostname: "nats"
};
