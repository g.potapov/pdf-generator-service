import { Rfi } from '../../shared/entity/Rfi';
import { DiaryReport } from '../../shared/entity/Diary/DiaryReport';
import { ProjectRole } from '../../shared/entity/Role/ProjectRole';

export type TimersManageInfoMap =
    Map<string, TimerOptions>;

export interface TimerOptions {
    timerLogic: () => Promise<void>;
    firstIterDesc?: NodeJS.Timeout;
    intervalIterDesc?: NodeJS.Timeout;
}

export type TimerEntityType = Rfi | DiaryReport | ProjectRole;
