import { Module } from '@nestjs/common';
import { SendNtfHelperService } from './send.ntf.helper.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Event } from '../../shared/entity/Event';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Event,
    ]),
  ],
  providers: [SendNtfHelperService],
  exports: [SendNtfHelperService],
})
export class SendNtfHelperModule {}
