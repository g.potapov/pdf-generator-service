import { Injectable } from '@nestjs/common';
import { UsersServiceProxy } from '../../shared/proxies/UsersServiceProxy';
import { NotificationAddDto } from '../../shared/dto/common/notification/notification.add.dto';
import { NotificationVia } from '../../shared/enum/notification/NotificationVia';
import { InjectRepository } from '@nestjs/typeorm';
import { Project } from '../../shared/entity/Project';
import { Repository } from 'typeorm';
import { Event } from '../../shared/entity/Event';
import { TimerEntityType } from '../types';

@Injectable()
export class SendNtfHelperService {
    constructor(
        private readonly usersServiceProxy: UsersServiceProxy,
        @InjectRepository(Event)
        private readonly eventRepository: Repository<Event>,
    ) {}

    public async sendNotification<TEntity extends TimerEntityType>(
        eventName: string,
        project: Project,
        entity: TEntity,
        proprietary?: Record<string, string | number | Date>,
    ) {
        const event = await this.eventRepository.findOne({ where: { name: eventName } });

        const ntf = NotificationAddDto.create(
            event,
            project,
            null,
            NotificationVia.SYSTEM,
            entity,
            proprietary,
        );

        await this.usersServiceProxy.notification(null, null, null).add(ntf);
    }
}
