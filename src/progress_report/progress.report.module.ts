import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from '../shared/entity/Project';
import { TimerModule } from '../timer/timer.module';
import { SendNtfHelperModule } from '../common/sendNtfHelper/send.ntf.helper.module';
import { ProgressReportsTimersService } from './progress.report.service';
import { Activity } from '../shared/entity/Activity/Activity';
import { ProjectRole } from '../shared/entity/Role/ProjectRole';
import { ProgressReport } from '../shared/entity/ProgressReport/ProgressReport';
import { ActivityLog } from '../shared/entity/Diary/ActivityLog';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Project,
      Activity,
      ProjectRole,
      ProgressReport,
      ActivityLog,
    ]),
    TimerModule,
    SendNtfHelperModule,
  ],
  providers: [ProgressReportsTimersService],
  exports: [ProgressReportsTimersService],
})
export class ProgressReportsTimersModule {}
