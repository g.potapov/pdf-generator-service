import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Project } from '../shared/entity/Project';
import { SendNtfHelperService } from '../common/sendNtfHelper/send.ntf.helper.service';
import { ReportFeature } from '../shared/enum/common/ReportFeature';
import { Timer, TimerGetter } from '../TimersRegister';
import { Activity } from '../shared/entity/Activity/Activity';
import { ProjectRole } from '../shared/entity/Role/ProjectRole';
import { ActivityStatus } from '../shared/enum/activity/ActivityStatus';
import { ProgressReport } from '../shared/entity/ProgressReport/ProgressReport';
import { ActivityLog } from '../shared/entity/Diary/ActivityLog';
import { ProgressReportPeriod } from '../shared/enum/common/ProgressReportPeriod';
import * as moment from 'moment';
import { groupBy } from 'rxjs/internal/operators/groupBy';

@Injectable()
export class ProgressReportsTimersService extends TimerGetter {
    private static TIMER_PROGRESS_REPORT_REMINDER = 'progressReportReminder';
    private static TIMER_PROGRESS_REPORT_NOT_FILLED = 'progressReportNotFilled';

    constructor(
        @InjectRepository(Activity)
        private readonly activityRepository: Repository<Activity>,
        @InjectRepository(ProjectRole)
        private readonly roleRepository: Repository<ProjectRole>,
        @InjectRepository(ProgressReport)
        private readonly progressReportRepository: Repository<ProgressReport>,
        @InjectRepository(Project)
        private readonly projectRepository: Repository<Project>,
        private readonly sendNtfHelperService: SendNtfHelperService,
    ) {
        super();
        this.timerProgressReportReminder = this.timerProgressReportReminder.bind(this);
        this.timerProgressReportNotFilled = this.timerProgressReportNotFilled.bind(this);
    }

    @Timer
    private async timerProgressReportReminder() {
        const currentDate = new Date();

        const ownerRoleIds = (await this.activityRepository
            .createQueryBuilder('activity')
            .select('activity."ownerRoleId"')
            .innerJoin(qb => {
                return qb
                    .from(Project, 'project')
                    .select([
                        'project."id"',
                        'project."optionsProgressreportperiod"',
                    ])
                    .where('project.removed = :removed', { removed: false })
                    .andWhere(
                        'project."optionsReportfeature" = :optionsReportFeature',
                        { optionsReportFeature: ReportFeature.ProgressReport },
                    )
                    .andWhere(`(
                        project."optionsProgressreportperiod" = :daily
                        OR (project."optionsProgressreportperiod" = :weekly
                            AND project."optionsProgressreportweeklyon"= :optionsProgressreportweeklyon)
                        OR (project."optionsProgressreportperiod"= :monthly
                            AND project."optionsProgressreportmonthlyon"= :optionsProgressreportmonthlyon)
                        )`,
                        {
                            optionsProgressreportweeklyon: currentDate.getDay(),
                            optionsProgressreportmonthlyon: currentDate.getDate(),
                            daily: ProgressReportPeriod.Daily,
                            weekly: ProgressReportPeriod.Weekly,
                            monthly: ProgressReportPeriod.Monthly,
                        },
                    );
                },
                'project',
                'activity.projectId = project."id"',
            )
            .where(`(activity.status = :startedStatus
                        OR (activity.status = :notStartedStatus AND
                             CASE project."optionsProgressreportperiod"
                                WHEN :daily THEN activity.startDate = date_trunc('day', NOW())
                                WHEN :weekly THEN activity.startDate BETWEEN
                                    (date_trunc('day', NOW()) - '1 week'::interval + '1 day'::interval) AND date_trunc('day', NOW())
                                WHEN :monthly THEN activity.startDate BETWEEN
                                    (date_trunc('day', NOW()) - '1 month'::interval + '1 day'::interval) AND date_trunc('day', NOW())
                                END)
                    )`,
                {
                    startedStatus: ActivityStatus.STARTED,
                    notStartedStatus: ActivityStatus.NOT_STARTED,
                },
            )
            .groupBy('activity."ownerRoleId"')
            .having('activity."ownerRoleId" IS NOT NULL')
            .getRawMany())
            .map((o) => o.ownerRoleId);

        if (!ownerRoleIds.length) {
            return;
        }

        const projectRoles = await this.roleRepository.findByIds(
            ownerRoleIds,
            {
                relations: ['users', 'project', 'project.ownerCompany'],
            },
        );

        // tslint:disable-next-line: no-console
        console.log('progressReportReminder projectsRoles', ownerRoleIds);

        for (const projectRole of projectRoles) {
            await this.sendNtfHelperService.sendNotification(
                ProgressReportsTimersService.TIMER_PROGRESS_REPORT_REMINDER,
                projectRole.project,
                projectRole,
            );
        }
    }

    @Timer
    private async timerProgressReportNotFilled() {
        const currentDate = moment();

        const rawOwnerRolesIdsQuery = (q) => {
            return q
                .select([
                    'project."optionsProgressreportperiod"',
                    'max_rd_by_role.max_rd',
                    'activity."ownerRoleId"',
                ])
                .from(Activity, 'activity')
                .innerJoin(subQuery => {
                    return subQuery
                        .from(Project, 'project')
                        .select(`
                            project.id AS "projectId",
                            project."optionsProgressreportperiod"
                        `)
                        .where('project.removed = :removed', { removed: false })
                        .andWhere(
                            'project."optionsReportfeature" = :optionsReportFeature',
                            { optionsReportFeature: ReportFeature.ProgressReport },
                        )
                        .andWhere(`(
                            project."optionsProgressreportperiod" = :daily
                            OR (project."optionsProgressreportperiod" = :weekly
                                AND project."optionsProgressreportweeklyon"= :optionsProgressreportweeklyon)
                            OR (project."optionsProgressreportperiod"= :monthly
                                AND project."optionsProgressreportmonthlyon"= :optionsProgressreportmonthlyon)
                            )`,
                            {
                                optionsProgressreportweeklyon: currentDate.day() - 1 < 0 ? 6 : currentDate.day() - 1,
                                optionsProgressreportmonthlyon: currentDate.date() - 1 === 0
                                    ? currentDate.subtract(1, 'month').daysInMonth()
                                    : currentDate.date() - 1,
                                daily: ProgressReportPeriod.Daily,
                                weekly: ProgressReportPeriod.Weekly,
                                monthly: ProgressReportPeriod.Monthly,
                            },
                        );
                    },
                    'project',
                    'project."projectId" = activity."projectId"',
                )
                .leftJoin(qbb => {
                    return qbb
                        .from(ProgressReport, 'pr')
                        .select('pr."creatorId" AS cid, Max(pr.reportDate) as max_rd')
                        .groupBy('pr."creatorId"');
                    },
                'max_rd_by_role',
                'activity."ownerRoleId"=max_rd_by_role.cid',
                )
                .where(`(activity.status = :startedStatus
                            OR (activity.status = :notStartedStatus AND
                                 CASE project."optionsProgressreportperiod"
                                    WHEN :daily THEN activity.startDate = date_trunc('day', NOW()-'1 day'::interval)
                                    WHEN :weekly THEN activity.startDate
                                        BETWEEN date_trunc('day', NOW()) - '1 week'::interval
                                            AND date_trunc('day', NOW()-'1 day'::interval)
                                    WHEN :monthly THEN activity.startDate BETWEEN
                                        date_trunc('day', NOW()) - '1 month'::interval
                                            AND date_trunc('day', NOW()-'1 day'::interval)
                                    END)
                        )`,
                    {
                        startedStatus: ActivityStatus.STARTED,
                        notStartedStatus: ActivityStatus.NOT_STARTED,
                    },
                );
        };

        const ownerRoleIds = (await this.activityRepository
            .manager
            .connection
            .createQueryBuilder()
            .select('t."ownerRoleId"')
            .from(qb => rawOwnerRolesIdsQuery(qb), 't')
            .where('t.max_rd IS NULL')
            .orWhere(`(
                    t."optionsProgressreportperiod" = :daily AND date_trunc('day', NOW()) - t.max_rd = '2 days'::interval
                )`,
                { daily: ProgressReportPeriod.Daily },
            )
            .orWhere(`t."optionsProgressreportperiod" <> :daily AND t.max_rd <> date_trunc('day', NOW()-'1 day'::interval)`)
            .groupBy('t."ownerRoleId"')
            .getRawMany())
            .map((o) => o.ownerRoleId);

        if (!ownerRoleIds.length) {
            return;
        }

        const projectRoles = await this.roleRepository.findByIds(
            ownerRoleIds,
            {
                relations: ['users', 'project', 'project.ownerCompany'],
            },
        );

        // tslint:disable-next-line: no-console
        console.log('progressReportNotFilled projectsRoles', ownerRoleIds);

        for (const projectRole of projectRoles) {
            await this.sendNtfHelperService.sendNotification(
                ProgressReportsTimersService.TIMER_PROGRESS_REPORT_NOT_FILLED,
                projectRole.project,
                projectRole,
            );
        }
    }
}
