import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Raw } from 'typeorm';
import { Project } from '../shared/entity/Project';
import { DiaryReport } from '../shared/entity/Diary/DiaryReport';
import { SendNtfHelperService } from '../common/sendNtfHelper/send.ntf.helper.service';
import { ReportFeature } from '../shared/enum/common/ReportFeature';
import { Timer, TimerGetter } from '../TimersRegister';
import { UsersServiceProxy } from '../shared/proxies/UsersServiceProxy';
import { BufferPerformanceLogDto } from '../shared/dto/request/buffer/buffer.performance.log.dto';

@Injectable()
export class DailyDiaryTimersService extends TimerGetter {
    private static TIMER_DIARY_REPORT_APPROVAL_TIMEOUT = 'timerDiaryReportApprovalTimeOut';
    private static TIMER_DIARY_REPORT_REMINDER = 'timerDiaryReportReminder';
    private static TIMER_DIARY_REPORT_CREATION_TIMEOUT = 'timerDiaryReportCreationTimeOut';

    constructor(
        @InjectRepository(DiaryReport)
        private readonly diaryReportRepository: Repository<DiaryReport>,
        @InjectRepository(Project)
        private readonly projectRepository: Repository<Project>,
        private readonly sendNtfHelperService: SendNtfHelperService,
        private readonly usersServiceProxy: UsersServiceProxy,

    ) {
        super();
        this.timerDiaryReportReminder = this.timerDiaryReportReminder.bind(this);
        this.timerDiaryReportApprovalTimeOut = this.timerDiaryReportApprovalTimeOut.bind(this);
        this.timerDiaryReportCreationTimeOut = this.timerDiaryReportCreationTimeOut.bind(this);
        this.timerBufferRecalcDaily = this.timerBufferRecalcDaily.bind(this);
    }

   @Timer
    private async timerDiaryReportReminder() {
        const projects = await this.projectRepository.find({
            where: {
                removed: false,
                options: {
                    reportFeature: ReportFeature.DailyDiary,
                },
            },
            relations: ['ownerCompany'],
        });

        // tslint:disable-next-line: no-console
        console.log('diaryReportReminder projects', projects.map((p) => p.id));

        for (const project of projects) {
            await this.sendNtfHelperService.sendNotification(
                DailyDiaryTimersService.TIMER_DIARY_REPORT_REMINDER,
                project,
                null,
            );
        }
    }

    @Timer
    private async timerBufferRecalcDaily() {
        const projects = await this.projectRepository.find({
            where: {
                removed: false,
            },
            relations: ['ownerCompany'],
        });

        // tslint:disable-next-line: no-console
        console.log('recalc buffer projects', projects.map((p) => p.id));

        for (const project of projects) {
            const dto = new BufferPerformanceLogDto();
            dto.needLogging = true;

            await this.usersServiceProxy
                .bufferItems(String(project.id), null)
                .calcPerformance(dto);
        }
    }

    @Timer
    private async timerDiaryReportApprovalTimeOut() {
        const diaryReportsApprovalTimedOut =
            await this.diaryReportRepository.find({
                where: {
                    pmDecisionMade: false,
                    options: {
                        reportFeature: ReportFeature.DailyDiary,
                    },
                },
                relations: ['project', 'project.ownerCompany'],
            });

        // tslint:disable-next-line: no-console
        console.log('diaryReportsApprovalTimedOut', diaryReportsApprovalTimedOut.map((d) => d.id));

        for (const diaryReport of diaryReportsApprovalTimedOut) {
            await this.sendNtfHelperService.sendNotification(
                DailyDiaryTimersService.TIMER_DIARY_REPORT_APPROVAL_TIMEOUT,
                diaryReport.project,
                diaryReport,
            );
        }
    }

    @Timer
    private async timerDiaryReportCreationTimeOut() {
        const projectsReportNotCreationTimeOutIds = await this.diaryReportRepository
            .createQueryBuilder('diaryReport')
            .select('"projectId"')
            .groupBy('"projectId"')
            .having(`date_trunc('day', now()) - Max("reportDate") <= interval '1 day'`)
            .getRawMany();

        const findOptions = projectsReportNotCreationTimeOutIds.length
            ? {
                id: Raw( alias => `${alias} NOT IN (${projectsReportNotCreationTimeOutIds.map(obj => Number(obj.projectId)).join()})`),
            }
            : {

            };

        const projects = await this.projectRepository.find({
            where: {
                ...findOptions,
                removed: false,
                options: {
                    reportFeature: ReportFeature.DailyDiary,
                },
            },
            relations: ['ownerCompany'],
        });

        // tslint:disable-next-line: no-console{
        console.log('diaryReportCreationTimeOut projects', projects.map((p) => p.id));

        for (const project of projects) {
            await this.sendNtfHelperService.sendNotification(
                DailyDiaryTimersService.TIMER_DIARY_REPORT_CREATION_TIMEOUT,
                project,
                null,
            );
        }
    }
}
