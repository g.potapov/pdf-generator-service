import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from '../shared/entity/Project';
import { TimerModule } from '../timer/timer.module';
import { DiaryReport } from '../shared/entity/Diary/DiaryReport';
import { DailyDiaryTimersService } from './daily.diary.service';
import { SendNtfHelperModule } from '../common/sendNtfHelper/send.ntf.helper.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Project,
      DiaryReport,
    ]),
    TimerModule,
    SendNtfHelperModule,
  ],
  providers: [DailyDiaryTimersService],
  exports: [DailyDiaryTimersService],
})
export class DailyDiaryTimersModule {}
