import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/common/enums/transport.enum';
import { DtoTransformPipe } from './shared/DtoTransform.pipe';
import { ValidationPipe } from '@nestjs/common';
import * as NatsCfg from '../nats_cfg';
import { AllExceptionsFilter } from './exceptions/all.exception.filter';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(
    AppModule,
    {
      transport: Transport.NATS,
      options: {
        url: `nats://${NatsCfg.hostname}:${NatsCfg.port}`,
      },
    });
  app.useGlobalPipes(
    new DtoTransformPipe(),
    new ValidationPipe(),
  );
  app.useGlobalFilters(new AllExceptionsFilter());
  app.listen(() => console.log('Microservice started!'));
}
bootstrap();
