import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RfiTimersService } from './rfi.service';
import { Rfi } from '../shared/entity/Rfi';
import { Project } from '../shared/entity/Project';
import { TimerModule } from '../timer/timer.module';
import { SendNtfHelperModule } from '../common/sendNtfHelper/send.ntf.helper.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Rfi,
      Project,
    ]),
    TimerModule,
    SendNtfHelperModule,
  ],
  providers: [RfiTimersService],
  exports: [RfiTimersService],
})
export class RfiTimersModule {}
