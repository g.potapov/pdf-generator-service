import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Rfi } from '../shared/entity/Rfi';
import { Project } from '../shared/entity/Project';
import { SendNtfHelperService } from '../common/sendNtfHelper/send.ntf.helper.service';
import { Company } from '../shared/entity/Company';
import { RfiOwnership } from '../shared/entity/Workflow/RfiOwnership';
import { Timer, TimerGetter } from '../TimersRegister';

@Injectable()
export class RfiTimersService extends TimerGetter {
    private static TIMER_RFI_APPROVAL_TIMEOUT = 'timerRFIApprovalTimeOut';
    private static TIMER_RFI_STATE_TIMEOUT = 'timerRFIStateTimeOut';
    constructor(
        @InjectRepository(Rfi)
        private readonly rfiRepository: Repository<Rfi>,
        private readonly sendNtfHelperService: SendNtfHelperService,
    ) {
        super();
        this.timerRFIApprovalTimeOut = this.timerRFIApprovalTimeOut.bind(this);
        this.timerRFIStateTimeOut = this.timerRFIStateTimeOut.bind(this);
    }

    @Timer
    private async timerRFIApprovalTimeOut() {
        const notApprovedRfis = await this.rfiRepository
            .createQueryBuilder('rfi')
            .innerJoinAndMapOne(
                'rfi.currentOwnership',
                RfiOwnership,
                'currentOwnership',
                'rfi."currentOwnershipId" = currentOwnership.id',
            )
            .innerJoinAndMapOne('rfi.project', Project, 'project', 'rfi.projectId = project.id')
            .innerJoinAndMapOne(
                'project.ownerCompany',
                Company,
                'ownerCompany',
                'project.ownerCompanyId = ownerCompany.id',
            )
            .where('rfi.status<>:status', { status: 'closed' })
            .andWhere(`(rfi.creationDate + make_interval(0, 0, 0, 0, project.autoCloseRfi, 0) < date_trunc('day', now()))`)
            .getMany();

        // tslint:disable-next-line: no-console
        console.log('notApprovedRfis', notApprovedRfis.map((rfi) => rfi.id));

        for (const rfi of notApprovedRfis) {
            await this.sendNtfHelperService.sendNotification(
                RfiTimersService.TIMER_RFI_APPROVAL_TIMEOUT,
                rfi.project,
                rfi,
            );
        }
    }

    @Timer
    private async timerRFIStateTimeOut() {
        const stateChangedRfis = await this.rfiRepository.createQueryBuilder('rfi')
            .innerJoinAndMapOne(
                'rfi.currentOwnership',
                RfiOwnership,
                'currentOwnership',
                'rfi."currentOwnershipId" = currentOwnership.id',
            )
            .innerJoinAndMapOne('rfi.project', Project, 'project', 'rfi.projectId = project.id')
            .innerJoinAndMapOne(
                'project.ownerCompany',
                Company,
                'ownerCompany',
                'project.ownerCompanyId = ownerCompany.id',
            )
            .where('rfi.status<>:status', { status: 'closed' })
            .andWhere(`((rfi.changedOwnerAt - rfi.creationDate > interval '1 day') OR rfi.changedOwnerTimesCount = 1)`)
            .getMany();

        // tslint:disable-next-line: no-console
        console.log('stateNotChangedRfis', stateChangedRfis.map((rfi) => rfi.id));

        for (const rfi of stateChangedRfis) {
            await this.sendNtfHelperService.sendNotification(
                RfiTimersService.TIMER_RFI_STATE_TIMEOUT,
                rfi.project,
                rfi,
            );
        }
    }
}
