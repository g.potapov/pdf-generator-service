import { Controller } from '@nestjs/common';
import { TimersManagerService } from './timers.manager.service';
import { MessagePattern } from '@nestjs/microservices';
import { SwitchModeDto } from '../shared/dto/request/timers/switch.mode.dto';
import { TimersServiceDto } from '../shared/dto/request/base/timers-service.dto';

@Controller()
export class TimersManagerController {
  constructor(private readonly timersManagerService: TimersManagerService) { }

  @MessagePattern({ cmd: 'switch_timers_mode' })
  switchTimersMode(data: TimersServiceDto<SwitchModeDto>): Promise<{status: 'OK'}> {
        this.timersManagerService.switchTimersMode(data);
        return Promise.resolve({status: 'OK'});
  }
}
