import { TimerService } from '../timer/timer.service';
import { Injectable } from '@nestjs/common';
import { RfiTimersService } from '../rfi/rfi.service';
import { DailyDiaryTimersService } from '../daily_diary/daily.diary.service';
import { TimersManageInfoMap } from '../common/types';
import { SwitchModeDto } from '../shared/dto/request/timers/switch.mode.dto';
import { WorkMode } from '../shared/enum/timers/WorkMode';
import { TimersServiceDto } from '../shared/dto/request/base/timers-service.dto';
import { ProgressReportsTimersService } from '../progress_report/progress.report.service';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class TimersManagerService {
    private timersManageInfoMap: TimersManageInfoMap;
    private currentMode!: WorkMode;
    private currentDevInterval!: number;

    constructor(
        private timer: TimerService,
        private configService: ConfigService,
        private rfiTimersService: RfiTimersService,
        private dailyDiaryTimersService: DailyDiaryTimersService,
        private progressReportTimersService: ProgressReportsTimersService,
    ) {
        const timerLogicByName = [
            this.dailyDiaryTimersService,
            this.rfiTimersService,
            this.progressReportTimersService,
        ].reduce(
            (a, c) => a.concat(c.getTimers().map(t => [t.name, { timerLogic: t }])),
            [],
        );

        this.timersManageInfoMap = new Map(timerLogicByName);
        this.timer.setTimerManagerInfoMap(this.timersManageInfoMap);

        this.runTimers(this.configService.get('LOCAL_RUN') ? WorkMode.DEV : WorkMode.PROD);
    }

    public switchTimersMode({ dto }: TimersServiceDto<SwitchModeDto>) {
        console.log(
            `Request! Got change timers mode to ${dto.mode}
            ${dto.dev_interval && dto.mode === WorkMode.DEV ? `, dev interval ${dto.dev_interval} min` : ','}
            previous mode is ${this.currentMode}`,
        );
        if (dto.mode === this.currentMode && dto.dev_interval === this.currentDevInterval) {
            return;
        }
        this.clear();
        this.runTimers(dto.mode, dto.dev_interval);
    }

    private clear() {
        this.timersManageInfoMap.forEach((options) => this.timer.removeTimer(options));
    }

    private runTimers(mode: WorkMode, devInterval?: number) {
        this.timersManageInfoMap.forEach(
            ({ timerLogic }) => {
                this.timer.setupTimer(timerLogic, mode, devInterval);
            },
        );
        this.currentMode = mode;
        this.currentDevInterval = devInterval;
    }
}
