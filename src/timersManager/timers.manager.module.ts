import { Module } from '@nestjs/common';
import { TimerModule } from '../timer/timer.module';
import { TimersManagerService } from './timers.manager.service';
import { RfiTimersModule } from '../rfi/rfi.module';
import { DailyDiaryTimersModule } from '../daily_diary/daily.diary.module';
import { TimersManagerController } from './timers.manager.controller';
import { ProgressReportsTimersModule } from '../progress_report/progress.report.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    TimerModule,
    RfiTimersModule,
    DailyDiaryTimersModule,
    ProgressReportsTimersModule,
    ConfigModule,
  ],
  controllers: [TimersManagerController],
  providers: [TimersManagerService],
  exports: [TimersManagerService],
})
export class TimersManagerModule {}
