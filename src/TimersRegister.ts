// use to mark function as a buisness timer
export function Timer(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const symbol = Symbol.for('Timer.TimerFunctions');
    if (!target[symbol]) {
            target[symbol] = [];
        }
    target[symbol].push(propertyKey);
}

export abstract class TimerGetter {
    public getTimers() {
        if (!this[Symbol.for('Timer.TimerFunctions')]) {
            return [];
        }
        return this[Symbol.for('Timer.TimerFunctions')].map(name => this[name]);
    }
}
