import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { throwError } from 'rxjs';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const code = exception.status;
    const message = exception.message;
    console.log('Got exc:', exception);
    return throwError({ code, message });
  }
}
