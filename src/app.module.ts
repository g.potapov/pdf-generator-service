import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProxiesModule } from './shared/proxies/proxies.module';
import * as natsCfg from '../nats_cfg.js';
import { TimersManagerModule } from './timersManager/timers.manager.module';
import { DtoModule } from './shared/dto/dto.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    ConfigModule.forRoot({
      envFilePath: '.local.env',
    }),
    ProxiesModule.register({ natsHostname: natsCfg.hostname, natsPort: natsCfg.port }),
    TimersManagerModule,
    DtoModule,
  ],
})
export class AppModule {}
