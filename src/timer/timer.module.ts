import { Module } from '@nestjs/common';
import { TimerService } from './timer.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ConfigModule],
  providers: [TimerService],
  exports: [TimerService],
})
export class TimerModule {}
