import { Injectable } from '@nestjs/common';
import * as moment from 'moment';
import { TimersManageInfoMap, TimerOptions } from '../common/types';
import { WorkMode } from '../shared/enum/timers/WorkMode';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class TimerService {
    private static INTERVAL_DAY_MS = 86400 * 1000;

    // ref from timerManager, need to async set setInterval/setTimeout
    // descriptor for process logic in Manager
    private timerManagerInfoMap!: TimersManageInfoMap;

    constructor(private configService: ConfigService) {
    }

    public setTimerManagerInfoMap(timerManagerInfoMap: TimersManageInfoMap) {
        this.timerManagerInfoMap = timerManagerInfoMap;
    }

    public setupTimer(
        timerLogic: () => Promise<void>,
        workMode: WorkMode,
        devInterval?: number,
    ) {
        let firstDelay;
        let intervalDelay;
        switch (workMode) {
            case WorkMode.DEV:
                firstDelay = intervalDelay =
                    devInterval * 60 * 1000 || (this.configService.get('LOCAL_RUN') ? 5 * 1000 : 15 * 60 * 1000);
                break;
            case WorkMode.PROD:
                firstDelay = this.firstIterationTimeLeft();
                intervalDelay = TimerService.INTERVAL_DAY_MS;
                break;
        }

        const firstIterDesc = setTimeout(
            async () => {
                const intervalIterDesc =
                    setInterval(
                        async () => await this.timerHandleHelper(timerLogic),
                        intervalDelay,
                    );
                this.timerManagerInfoMap.get(timerLogic.name).intervalIterDesc = intervalIterDesc;
                await this.timerHandleHelper(timerLogic);
            },
            firstDelay,
        );

        this.timerManagerInfoMap.get(timerLogic.name).firstIterDesc = firstIterDesc;
    }

    public removeTimer({ firstIterDesc, intervalIterDesc }: TimerOptions) {
        if (firstIterDesc) {
            clearTimeout(firstIterDesc);
        }
        if (intervalIterDesc) {
            clearInterval(intervalIterDesc);
        }
    }

    private firstIterationTimeLeft() {
        const currentDate = moment();

        let firstRunEnd;
        if (currentDate.isBefore(currentDate.clone().startOf('day').add(1, 'hours'))) {
            firstRunEnd = currentDate.clone().startOf('day').add(1, 'hours');
        } else {
            firstRunEnd = currentDate.clone().endOf('day').add(1, 'hours');
        }

        return firstRunEnd.diff(currentDate, 'seconds').valueOf() * 1000;
    }

    private async timerHandleHelper(timerLogic: () => Promise<void>) {
        try {
            await timerLogic();
        } catch (e) {
            // tslint:disable-next-line: no-console
            console.log(`Error through running ${timerLogic.name} timer, reason:`);
            // tslint:disable-next-line: no-console
            console.log(e);
        }
    }
}
